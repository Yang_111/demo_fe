set -ex

if [ ! -n "$1" ]; then
VER=`git tag | grep -v "v" | tail -n 1 | python3 -c "import sys; vers = sys.stdin.readline().strip().split('.'); print('.'.join([*vers[:2], str(int(vers[2])+1)]))"`
if [ -z "$VER" ]; then
VER="0.0.1"
fi
else
VER="$1"
fi

echo "使用的版本号为：$VER"

git tag $VER
git push -u origin ${VER}

docker build -t crpi-od1rzn9qkn8g9vof.cn-guangzhou.personal.cr.aliyuncs.com/ylt666/demo_fe:$VER .
docker push crpi-od1rzn9qkn8g9vof.cn-guangzhou.personal.cr.aliyuncs.com/ylt666/demo_fe:$VER
echo crpi-od1rzn9qkn8g9vof.cn-guangzhou.personal.cr.aliyuncs.com/ylt666/demo_fe:$VER
