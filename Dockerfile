FROM alpine AS builder
RUN sed -i "s/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g" /etc/apk/repositories \
&& apk add --no-cache nodejs npm
COPY . /src/fe-demo
WORKDIR /src/fe-demo
RUN npm --registry=https://registry.npmmirror.com install && npm run build

FROM nginx AS prod
COPY --from=builder /src/fe-demo/dist /usr/share/nginx/html
